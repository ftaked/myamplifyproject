/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateMessage = `subscription OnCreateMessage {
  onCreateMessage {
    id
    username
    content
  }
}
`;
export const onUpdateMessage = `subscription OnUpdateMessage {
  onUpdateMessage {
    id
    username
    content
  }
}
`;
export const onDeleteMessage = `subscription OnDeleteMessage {
  onDeleteMessage {
    id
    username
    content
  }
}
`;
