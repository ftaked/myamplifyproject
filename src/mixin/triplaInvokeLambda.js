import axios from 'axios'

export default {
  methods: {
    /**
    * API Gatewayにアクセスする共通関数
    * @param invokeUrl : API Gatewayのエンドポイント
    * @param params : Axios通信する際のパラメータ指定
    * @return data : Lambda関数を実行した際に返却される値
    */
    invokeLambda (invokeUrl, params) {
      return new Promise(function (resolve, reject) {
        axios.get(
          invokeUrl,
          {
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            crossDomain: true,
            params: params
          }
        ).then(res => {
          return resolve(res)
        }).catch(err => {
          console.error(err)
          reject(err)
        })
      })
    }
  }
}
