import Vue from 'vue';
import Router from 'vue-router';
import AI from './AI.vue';
import Chat from './Chat.vue';
import Index from './00_Index.vue';
import Login from './01_Login.vue';
import Entry from './02_Entry.vue';
import Serch from './03_Serch.vue';
import Trip from './04_Trip.vue';
import Account from './99_Account.vue';


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/ai',
      name: 'ai',
      component: AI
    },
    {
      path: '/chat',
      name: 'chat',
      component: Chat
    },
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/entry',
      name: 'entry',
      component: Entry
    },
    {
      path: '/serch',
      name: 'serch',
      component: Serch
    },
    {
      path: '/trip',
      name: 'trip',
      component: Trip
    },
    {
      path: '/Account',
      name: 'Account',
      component: Account
    }
  ]
});
